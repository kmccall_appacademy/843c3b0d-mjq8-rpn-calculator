class RPNCalculator
  def initialize
    @stack = []
    @tokens = []
    @operators = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.size < 2

    second_operand = @stack.pop
    first_operand = @stack.pop

    case op_symbol
    when :+
      @stack << first_operand + second_operand
    when :-
      @stack << first_operand - second_operand
    when :*
      @stack << first_operand * second_operand
    when :/
      @stack << first_operand.to_f / second_operand.to_f

    else
      @stack << first_operand
      @stack << second_operand
      raise "No such operation: #{op_symbol}"
    end
  end

  def tokens(string)
    tokens = string.split
    operators = "+-/*"
    tokens.map do |ch|
      if ch.to_i.to_s == ch
        @tokens << ch.to_i
      elsif operators.include?(ch)
        @tokens << ch.to_sym
      end
    end
    @tokens
  end

  def evaluate(string)
    tokens(string).each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

end
